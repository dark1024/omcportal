﻿using System;
using System.Web.Mvc;
using OMCPortal.Models;

namespace OMCPortal.Controllers
{
    public class OMCController : Controller
    {
        string version = "1.0.0.7 Beta";
        OMCInstances instances = new OMCInstances();

        public ActionResult Index()
        {
            if (Session["Logged"] == null)
                return RedirectToAction("Login");
            ViewBag.version = version;
            return View();
        }

        public ActionResult Dashboard()
        {
            if (Session["Logged"] == null)
                return RedirectToAction("Login");
            instances.Refresh();
            ViewBag.data = instances.instances;
            ViewBag.version = version;
            return View();
        }

        public ActionResult Details( int id )
        {
            if (Session["Logged"] == null)
                return RedirectToAction("Login");
            ViewBag.data = null;
            ViewBag.version = version;
            instances.Refresh();
            if (id != -1)
            {
                foreach (var item in instances.instances)
                {
                    if (item == null)
                        continue;
                    if (item.InstanceID == id)
                    {
                        ViewBag.data = item;
                        break;
                    }
                }
            }
            return View();
        }

        public ActionResult LastErrors(int id)
        {
            if (Session["Logged"] == null)
                return RedirectToAction("Login");
            instances.ReloadErrors(id);
            instances.Refresh();
            ViewBag.data = instances.errors;
            ViewBag.sitecd = instances.SiteCD(id);
            ViewBag.version = version;
            return View();
        }

        public ActionResult LastMessages(int id)
        {
            if (Session["Logged"] == null)
                return RedirectToAction("Login");
            instances.ReloadMessages(id);
            instances.Refresh();            
            ViewBag.data = instances.messages;
            ViewBag.sitecd = instances.SiteCD(id);
            ViewBag.version = version;
            return View();
        }

        [HttpPost]
        public JsonResult Refresh()
        {
            if (Session["Logged"] == null)
                return null;
            instances.Refresh();
            JsonResult result = Json(instances.instances, JsonRequestBehavior.AllowGet);
            return result;
        }

        public ActionResult History( int id )
        {
            if (Session["Logged"] == null)
                return RedirectToAction("Login");
            instances.ReloadHistory(id);
            ViewBag.data = instances.instancesHistory;
            ViewBag.sitecd = instances.SiteCD(id);
            ViewBag.version = version;
            return View();
        }

        public ActionResult Orders(int id)
        {
            if (Session["Logged"] == null)
                return RedirectToAction("Login");
            DateTime dt = DateTime.Now;
            String siteCD = instances.SiteCD(id);
            instances.ReloadOrders(siteCD, dt);
            ViewBag.data = instances.orders;
            ViewBag.sitecd = siteCD;
            ViewBag.version = version;
            ViewBag.year = dt.Year;
            ViewBag.month = dt.Month;
            ViewBag.day = dt.Day;
            return View();
        }

        public ActionResult Login()
        {
            ViewBag.version = version;
            return View();
        }

        public ActionResult Logout()
        {
            Session["Logged"] = null;
            Session["UserName"] = null;
            return RedirectToAction("Login");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(OMCLoginModel objUser)
        {
            if (ModelState.IsValid)
            {
                if (objUser.LoginOK())
                {
                    Session["UserName"] = objUser.UserName;
                    Session["Logged"] = "TRUE";
                    return RedirectToAction("Dashboard");
                }
                else
                    return RedirectToAction("Login");
            }
            return View(objUser);
        }

        [HttpPost]
        public JsonResult LoadOrders(String siteCD, String year, String month, String day)
        {
            if( String.IsNullOrEmpty(year) || String.IsNullOrEmpty(month) || String.IsNullOrEmpty(day) || String.IsNullOrEmpty(siteCD))            
                return Json(null, JsonRequestBehavior.AllowGet);
            DateTime dt = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));
            instances.ReloadOrders(siteCD, dt);
            JsonResult result = Json(instances.orders, JsonRequestBehavior.AllowGet);
            String str = result.ToString();
            return result;
        }

        [HttpPost]
        public JsonResult FindOrder(String siteCD, String dms_ident)
        {
            if (String.IsNullOrEmpty(dms_ident) || String.IsNullOrEmpty(siteCD))
                return Json(null, JsonRequestBehavior.AllowGet);
            instances.FindOrder(siteCD, dms_ident);
            JsonResult result = Json(instances.orders, JsonRequestBehavior.AllowGet);
            String str = result.ToString();
            return result;
        }

    }
}
