﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace OMCPortal.Models
{
    public class OMCInstance
    {
        private int laRedThr = -10;
        private int laYellowThr = -3;
        private int leRedThr = -1 * 30;
        private int leYellowThr = -2 * 60;
        private int instanceID = -1;
        private String instanceSiteCD = "";
        private String instanceName = "";
        private String instanceDesc = "";
        private String instanceIP = "";
        private String instanceHost = "";
        private DateTime instanceRegistered = new DateTime();
        private DateTime instanceUnregistered = new DateTime();
        private String instanceLastAlive = "";
        private String instanceLastError = "";
        private String instanceLastMessage = "";
        private String instanceLastOrder = "";

        public int InstanceID { get { return instanceID; } set { instanceID = value; } }
        public String SiteCD { get { return instanceSiteCD; } set { instanceSiteCD = value; } }
        public String Name { get { return instanceName; } set { instanceName = value; } }
        public String Desc { get { return instanceDesc; } set { instanceDesc = value; } }
        public String IP { get { return instanceIP; } set { instanceIP = value; } }
        public String Host { get { return instanceHost; } set { instanceHost = value; } }
        public DateTime Registered { get { return instanceRegistered; } set { instanceRegistered = value; } }
        public DateTime Unregistered { get { return instanceUnregistered; } set { instanceUnregistered = value; } }
        public String LastAlive { get { return instanceLastAlive; } set { instanceLastAlive = value; } }
        public String LastError { get { return instanceLastError; } set { instanceLastError = value; } }
        public String LastMessage { get { return instanceLastMessage; } set { instanceLastMessage = value; } }
        public String LastOrder { get { return instanceLastOrder; } set { instanceLastOrder = value; } }
        public String LastAliveImage
        {
            get
            {
                if( LastAlive == "-")
                    return "GRAY";
                DateTime dt;
                if (DateTime.TryParse(LastAlive, out dt))
                {
                    if( dt < DateTime.Now.AddMinutes(laRedThr))
                        return "RED";
                    if (dt < DateTime.Now.AddMinutes(laYellowThr))
                        return "YELLOW";
                }
                else
                    return "GRAY";
                return "GREEN";
            }
        }
        public String LastErrorImage
        {
            get
            {
                if (LastError == "-")
                    return "GRAY";
                DateTime dt;
                if (DateTime.TryParse(LastError, out dt))
                {
                    if (dt > DateTime.Now.AddMinutes(leRedThr))
                        return "RED";
                    if (dt > DateTime.Now.AddMinutes(leYellowThr))
                        return "YELLOW";
                }
                else
                    return "GRAY";
                return "GREEN";
            }
        }
    }

    public class OMCError
    {
        private DateTime timestamp;
        private String msg;

        public DateTime Timestamp { get { return timestamp; } set { timestamp = value; } }
        public String Msg { get { return msg; } set { msg = value; } }
    }

    public class OMCMessage
    {
        private DateTime timestamp;
        private String msg;

        public DateTime Timestamp { get { return timestamp; } set { timestamp = value; } }
        public String Msg { get { return msg; } set { msg = value; } }
    }

    public class OMCOrder
    {
        private DateTime timestamp;
        private String dmsRef;
        private String dmsSubRef;

        public DateTime Timestamp { get { return timestamp; } set { timestamp = value; } }
        public String DmsRef { get { return dmsRef; } set { dmsRef = value; } }
        public String DmsSubRef { get { return dmsSubRef; } set { dmsSubRef = value; } }
    }

    public class OMCInstances
    {
        private String dbName = @"";
        private String dbUser = @"";
        private String dbPassword = @"";
        private String dbHost = @"";
        private String dbServerInstance = @"";
        private String dbPort = "";

        private int lastErrorsCount = 50;
        private int lastMessagesCount = 100;

        public int LastErrorsCount { get { return lastErrorsCount; } set { lastErrorsCount = value; } }
        public int LastMessagesCount { get { return lastMessagesCount; } set { lastMessagesCount = value; } }

        public List<OMCInstance> instances = new List<OMCInstance>();
        public List<OMCError> errors = new List<OMCError>();
        public List<OMCMessage> messages = new List<OMCMessage>();
        public List<OMCInstance> instancesHistory = new List<OMCInstance>();
        public List<OMCOrder> orders = new List<OMCOrder>();

        public String SiteCD( int id )
        {
            Refresh();
            foreach( var item in instances )
            {
                if (item == null)
                    continue;
                if (item.InstanceID == id)
                    return item.SiteCD;
            }
            return "";
        }

        private void ReadSettings()
        {
            dbHost = WebConfigurationManager.AppSettings["DBHost"];
            dbServerInstance = WebConfigurationManager.AppSettings["DBInstance"];
            dbName = WebConfigurationManager.AppSettings["DBName"];
            dbUser = WebConfigurationManager.AppSettings["DBUser"];
            dbPassword = WebConfigurationManager.AppSettings["DBPassword"];
            dbPort = WebConfigurationManager.AppSettings["DBPort"];
        }

        private String ConnectionString()
        {
            String server = dbHost + dbServerInstance;
            String connectionString =
            "Data Source=" + server + ( !String.IsNullOrEmpty(dbPort) ? "," + dbPort : "") + ";" +
            "Initial Catalog=" + dbName + ";" +
            "User id=" + dbUser + ";" +
            "Password=" + dbPassword + ";";
            return connectionString;
        }

        public void Refresh()
        {
            instances.Clear();
            ReadSettings();
            String connectionString = ConnectionString();
            String sql =
      @"SELECT T.[INSTANCE_ID]
      ,T.[OBJECT_ID]
      ,T.[INSTANCE_SITECD]
      ,T.[INSTANCE_NAME]
      ,T.[INSTANCE_DESC]
      ,T.[INSTANCE_IP]
      ,T.[INSTANCE_HOST]
      ,T.[REGISTERED]
      ,T.[UNREGISTERED]
      ,T.[LICENSE]
      ,T.[LAST_ALIVE]
      ,T.[LAST_ERROR]
      ,T.[LAST_MESSAGE]
      ,(SELECT max(O1.[TIMESTAMP])
                            FROM [dbo].[Orders] as O1 INNER JOIN [dbo].[Instances] as T1 on O1.[INSTANCE_ID] = T1.[INSTANCE_ID]
                            WHERE T1.[INSTANCE_SITECD] = T.[INSTANCE_SITECD] group by T1.[INSTANCE_SITECD]) as [LAST_ORDER]
  FROM [WINGS_OMC].[dbo].[Instances] as T   
  WHERE T.[LAST_ALIVE] = 
  (SELECT MAX(T1.[LAST_ALIVE]) FROM [WINGS_OMC].[dbo].[Instances] as T1 WHERE T.[INSTANCE_SITECD] = T1.[INSTANCE_SITECD] GROUP BY T1.[INSTANCE_SITECD])  
  ORDER BY T.[INSTANCE_SITECD];";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                SqlDataReader reader = null;
                try
                {
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            OMCInstance newInst = new OMCInstance();
                            if (!reader.IsDBNull(0))
                                newInst.InstanceID = reader.GetInt32(0);
                            if (!reader.IsDBNull(2))
                                newInst.SiteCD = reader.GetString(2);
                            if (!reader.IsDBNull(3))
                                newInst.Name = reader.GetString(3);
                            if (!reader.IsDBNull(4))
                                newInst.Desc = reader.GetString(4);
                            if (!reader.IsDBNull(5))
                                newInst.IP = reader.GetString(5);
                            if (!reader.IsDBNull(6))
                                newInst.Host = reader.GetString(6);
                            newInst.Registered = reader.GetDateTime(7);
                            if (!reader.IsDBNull(8))
                                newInst.Unregistered = reader.GetDateTime(8);
                            if (!reader.IsDBNull(10))
                                newInst.LastAlive = reader.GetDateTime(10).ToString("yyyy-MM-dd HH:mm:ss");
                            else
                                newInst.LastAlive = "-";
                            if (!reader.IsDBNull(11))
                                newInst.LastError = reader.GetDateTime(11).ToString("yyyy-MM-dd HH:mm:ss");
                            else
                                newInst.LastError = "-";
                            if (!reader.IsDBNull(12))
                                newInst.LastMessage = reader.GetDateTime(12).ToString("yyyy-MM-dd HH:mm:ss");
                            else
                                newInst.LastMessage = "-";
                            if (!reader.IsDBNull(13))
                                newInst.LastOrder = reader.GetDateTime(13).ToString("yyyy-MM-dd HH:mm:ss");
                            else
                                newInst.LastOrder = "-";
                            instances.Add(newInst);
                        }
                    }
                }
                catch
                {
                }
                finally
                {
                    if ((reader != null) && !reader.IsClosed)
                        reader.Close();
                }

            }
        }

        public void ReloadErrors( int id )
        {
            errors.Clear();
            ReadSettings();
            String connectionString = ConnectionString();
            String sql = String.Format(
                        @"SELECT TOP {0} 
                          [ERROR_TIMESTAMP]
                          ,[ERROR_MESSAGE]
                        FROM[WINGS_OMC].[dbo].[Errors]
                        WHERE [INSTANCE_ID] = {1} ORDER BY [ERROR_TIMESTAMP] DESC; ", LastErrorsCount, id );
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                SqlDataReader reader = null;
                try
                { 
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            OMCError newError = new OMCError();
                            if (!reader.IsDBNull(0))
                                newError.Timestamp= reader.GetDateTime(0);
                            if (!reader.IsDBNull(1))
                                newError.Msg = reader.GetString(1);
                            errors.Add(newError);
                        }
                    }
                }
                catch
                { }
                finally
                {
                    if ((reader != null) && !reader.IsClosed)
                        reader.Close();
                }
            }
        }

        public void ReloadMessages(int id)
        {
            messages.Clear();
            ReadSettings();
            String connectionString = ConnectionString();
            String sql = String.Format(
                        @"SELECT TOP {0} 
                          [MESSAGE_TIMESTAMP]
                          ,[MESSAGE_TEXT]
                        FROM[WINGS_OMC].[dbo].[Messages]
                        WHERE [INSTANCE_ID] = {1} ORDER BY [MESSAGE_TIMESTAMP] DESC; ", LastMessagesCount, id);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                SqlDataReader reader = null;
                try
                { 
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            OMCMessage newMsg = new OMCMessage();
                            if (!reader.IsDBNull(0))
                                newMsg.Timestamp = reader.GetDateTime(0);
                            if (!reader.IsDBNull(1))
                                newMsg.Msg = reader.GetString(1);
                            messages.Add(newMsg);
                        }
                    }
                }
                catch
                { }
                finally
                {
                    if ((reader != null) && !reader.IsClosed)
                        reader.Close();
                }
            }
        }

        public void ReloadOrders(String siteCD)
        {
            orders.Clear();
            ReadSettings();
            String connectionString = ConnectionString();
            String sql = String.Format(
                            @"SELECT [TIMESTAMP]
                                      ,[DMSREF]
                                      ,[DMSSUBREF]
                            FROM [dbo].[Orders] INNER JOIN [dbo].[Instances] on [dbo].[Orders].[INSTANCE_ID] = [dbo].[Instances].[INSTANCE_ID]
                            WHERE [dbo].[Instances].[INSTANCE_SITECD] = '{0}' 
                            ORDER BY [TIMESTAMP];",
                            siteCD);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                SqlDataReader reader = null;
                try
                {
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            OMCOrder newOrder = new OMCOrder();
                            if (!reader.IsDBNull(0))
                                newOrder.Timestamp = reader.GetDateTime(0);
                            if (!reader.IsDBNull(1))
                                newOrder.DmsRef = reader.GetString(1);
                            else
                                newOrder.DmsRef = "";
                            if (!reader.IsDBNull(2))
                                newOrder.DmsSubRef = reader.GetString(2);
                            else
                                newOrder.DmsSubRef = "";
                            orders.Add(newOrder);
                        }
                    }
                }
                catch
                { }
                finally
                {
                    if ((reader != null) && !reader.IsClosed)
                        reader.Close();
                }
            }
        }

        public void FindOrder(String siteCD, String ident)
        {
            orders.Clear();
            ReadSettings();
            String connectionString = ConnectionString();
            String sql = String.Format(
                            @"SELECT [TIMESTAMP]
                                      ,[DMSREF]
                                      ,[DMSSUBREF]
                            FROM [dbo].[Orders] INNER JOIN [dbo].[Instances] on [dbo].[Orders].[INSTANCE_ID] = [dbo].[Instances].[INSTANCE_ID]
                            WHERE [dbo].[Instances].[INSTANCE_SITECD] = '{0}' and ([DMSREF] = '{1}' or [DMSSUBREF] = '{1}')
                            ORDER BY [TIMESTAMP];",
                            siteCD, ident);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                SqlDataReader reader = null;
                try
                {
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            OMCOrder newOrder = new OMCOrder();
                            if (!reader.IsDBNull(0))
                                newOrder.Timestamp = reader.GetDateTime(0);
                            if (!reader.IsDBNull(1))
                                newOrder.DmsRef = reader.GetString(1);
                            else
                                newOrder.DmsRef = "";
                            if (!reader.IsDBNull(2))
                                newOrder.DmsSubRef = reader.GetString(2);
                            else
                                newOrder.DmsSubRef = "";
                            orders.Add(newOrder);
                        }
                    }
                }
                catch
                { }
                finally
                {
                    if ((reader != null) && !reader.IsClosed)
                        reader.Close();
                }
            }
        }


        public void ReloadOrders(String siteCD, DateTime dt)
        {
            orders.Clear();
            ReadSettings();
            String connectionString = ConnectionString();
            String sql = String.Format(
                            @"SELECT [TIMESTAMP]
                                      ,[DMSREF]
                                      ,[DMSSUBREF]
                            FROM [dbo].[Orders] INNER JOIN [dbo].[Instances] on [dbo].[Orders].[INSTANCE_ID] = [dbo].[Instances].[INSTANCE_ID]
                            WHERE [dbo].[Instances].[INSTANCE_SITECD] = '{0}' and 
                                DatePart(year, [TIMESTAMP])  = {1} and 
                                DatePart(month, [TIMESTAMP])  = {2} and 
                                DatePart(day, [TIMESTAMP])  = {3}
                            ORDER BY [TIMESTAMP];",
                            siteCD, dt.Year, dt.Month, dt.Day);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                SqlDataReader reader = null;
                try
                {
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            OMCOrder newOrder = new OMCOrder();
                            if (!reader.IsDBNull(0))
                                newOrder.Timestamp = reader.GetDateTime(0);
                            if (!reader.IsDBNull(1))
                                newOrder.DmsRef = reader.GetString(1);
                            else
                                newOrder.DmsRef = "";
                            if (!reader.IsDBNull(2))
                                newOrder.DmsSubRef = reader.GetString(2);
                            else
                                newOrder.DmsSubRef = "";
                            orders.Add(newOrder);
                        }
                    }
                }
                catch
                { }
                finally
                {
                    if ((reader != null) && !reader.IsClosed)
                        reader.Close();
                }
            }
        }

        public void ReloadHistory( int id )
        {
            String siteCD = SiteCD(id);
            instancesHistory.Clear();
            ReadSettings();
            String connectionString = ConnectionString();
            String sql = String.Format(
                        @"SELECT TOP 50 
                          [INSTANCE_ID]
                          ,[OBJECT_ID]
                          ,[INSTANCE_SITECD]
                          ,[INSTANCE_NAME]
                          ,[INSTANCE_DESC]
                          ,[INSTANCE_IP]
                          ,[INSTANCE_HOST]
                          ,[REGISTERED]
                          ,[UNREGISTERED]
                          ,[LICENSE]
                          ,[LAST_ALIVE]
                          ,[LAST_ERROR]
                          ,[LAST_MESSAGE]
                        FROM [WINGS_OMC].[dbo].[Instances]
                        WHERE [INSTANCE_SITECD] ='{0}' order by [REGISTERED] desc;", siteCD );
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                SqlDataReader reader = null;
                try
                {
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            OMCInstance newInst = new OMCInstance();
                            if (!reader.IsDBNull(0))
                                newInst.InstanceID = reader.GetInt32(0);
                            if (!reader.IsDBNull(2))
                                newInst.SiteCD = reader.GetString(2);
                            if (!reader.IsDBNull(3))
                                newInst.Name = reader.GetString(3);
                            if (!reader.IsDBNull(4))
                                newInst.Desc = reader.GetString(4);
                            if (!reader.IsDBNull(5))
                                newInst.IP = reader.GetString(5);
                            if (!reader.IsDBNull(6))
                                newInst.Host = reader.GetString(6);
                            newInst.Registered = reader.GetDateTime(7);
                            if (!reader.IsDBNull(8))
                                newInst.Unregistered = reader.GetDateTime(8);
                            if (!reader.IsDBNull(10))
                                newInst.LastAlive = reader.GetDateTime(10).ToString("yyyy-MM-dd HH:mm:ss");
                            else
                                newInst.LastAlive = "-";
                            if (!reader.IsDBNull(11))
                                newInst.LastError = reader.GetDateTime(11).ToString("yyyy-MM-dd HH:mm:ss");
                            else
                                newInst.LastError = "-";
                            if (!reader.IsDBNull(12))
                                newInst.LastMessage = reader.GetDateTime(12).ToString("yyyy-MM-dd HH:mm:ss");
                            else
                                newInst.LastMessage = "-";
                            instancesHistory.Add(newInst);
                        }
                    }
                }
                catch
                { }
                finally
                {
                    if ((reader != null) && !reader.IsClosed)
                        reader.Close();
                }
            }
        }
    }
}
