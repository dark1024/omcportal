﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.Configuration;
using System.IO;


namespace OMCPortal.Models
{
    public class OMCLoginModel
    {
        private String userName = "";
        private String password = "";

        private String dbName = @"";
        private String dbUser = @"";
        private String dbPassword = @"";
        private String dbHost = @"";
        private String dbServerInstance = @"";
        private String dbPort = "";

        private void ReadSettings()
        {
            dbHost = WebConfigurationManager.AppSettings["DBHost"];
            dbServerInstance = WebConfigurationManager.AppSettings["DBInstance"];
            dbName = WebConfigurationManager.AppSettings["DBName"];
            dbUser = WebConfigurationManager.AppSettings["DBUser"];
            dbPassword = WebConfigurationManager.AppSettings["DBPassword"];
            dbPort = WebConfigurationManager.AppSettings["DBPort"];
        }

        private String ConnectionString()
        {
            String server = dbHost + dbServerInstance;
            String connectionString =
            "Data Source=" + server + (!String.IsNullOrEmpty(dbPort) ? "," + dbPort : "") + ";" +
            "Initial Catalog=" + dbName + ";" +
            "User id=" + dbUser + ";" +
            "Password=" + dbPassword + ";";
            return connectionString;
        }


        public String UserName { set { userName = value; } get { return userName; } }
        public String Password { set { password = value; } get { return password; } }

        public bool LoginOK()
        {
            bool ret = false;
            if (Password == null || UserName == null)
                return false;
            String pwd = Convert.ToBase64String(Encoding.UTF8.GetBytes(Password));
            ReadSettings();
            String connectionString = ConnectionString();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("LoginUser", connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userName", UserName);
                cmd.Parameters.AddWithValue("@password", pwd);
                SqlDataReader reader = null;
                try
                {
                    connection.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                                if (reader.GetInt32(0) == 1)
                                {                                    
                                    ret = true;
                                    break;
                                }
                        }
                    }
                }
                catch(Exception e)
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"c:\Temp\log.txt"))
                    {
                        file.WriteLine(e.Message);
                    }
                }
                finally
                {
                    if ((reader != null) && !reader.IsClosed)
                        reader.Close();
                }
            }
            return ret;
        }
    }
}